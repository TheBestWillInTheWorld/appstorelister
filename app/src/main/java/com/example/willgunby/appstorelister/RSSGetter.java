package com.example.willgunby.appstorelister;

import android.os.AsyncTask;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

public class RSSGetter extends AsyncTask<String,Void,String> {

    private String myXmlData;
    private AsyncResponse responseHandler;
    private ArrayList<AppListing> apps;

    public ArrayList<AppListing> getApps() {
        return apps;
    }

    RSSGetter(AsyncResponse responseHandler){
        this.responseHandler = responseHandler;
        apps = new ArrayList<>();
    }

    public boolean processResult(String data){
        boolean opStatus = true;

        AppListing app = null;
        boolean inEntry = false;
        String textValue = "";

        XmlPullParserFactory factory;
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();

            parser.setInput(new StringReader(data));
            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = parser.getName();

                if (eventType == XmlPullParser.START_TAG)    {
                    if (tagName.equalsIgnoreCase("entry"))   {       app = new AppListing(); inEntry = true;  } }
                else if(eventType == XmlPullParser.TEXT)     {       textValue = parser.getText();            }
                else if(eventType == XmlPullParser.END_TAG)  {
                    if (inEntry && app != null)              {
                        if (tagName.equalsIgnoreCase("entry"))      {apps.add(app); inEntry = false;          }
                        if (tagName.equalsIgnoreCase("name"))        app.setName(textValue);
                        if (tagName.equalsIgnoreCase("artist"))      app.setArtist(textValue);
                        if (tagName.equalsIgnoreCase("releaseDate")) app.setReleaseDate(textValue);
                    }
                }
                eventType = parser.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            opStatus = false;
        } catch (IOException e) {
            e.printStackTrace();
            opStatus = false;
        }

        for (AppListing anApp :apps){
            Log.d("LOG", "**************");
            Log.d("LOG", anApp.getName());
            Log.d("LOG", anApp.getArtist());
            Log.d("LOG", anApp.getReleaseDate());
        }

        return opStatus;
    }

    @Override
    protected String doInBackground(String... params) {

        try {
            myXmlData = downloadXML(params[0]);
        }
        catch (IOException e){
            return "Unable to download XML content.";
        }

        return null;
    }

    protected void onPostExecute(String result){
        //Log.d("onPostExecute", myXmlData);
        boolean status = processResult(myXmlData);
        responseHandler.asyncResult(this, status);
    }


    private String downloadXML(String urlString) throws IOException {
        int BUFFER_SIZE    = 1000;
        InputStream input = null;
        String contents   = "";

        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            int response = conn.getResponseCode();
            Log.d("downloadXML", "HTTPResponseCode: "+ response);

            input = conn.getInputStream();

            InputStreamReader reader = new InputStreamReader(input);
            int charRead;
            char[] inputBuffer = new char[BUFFER_SIZE];

            while ((charRead = reader.read(inputBuffer))>0){
                String readString = String.copyValueOf(inputBuffer, 0, charRead);
                contents += readString;
                inputBuffer = new char[BUFFER_SIZE];
            }

            return contents;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return "Bad address.";
        } catch (ProtocolException e) {
            e.printStackTrace();
            return "Protocol Exception";
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            return "Unexpected error - " + e.getMessage();
        } finally {
            if(input != null) input.close();
        }

    }

}