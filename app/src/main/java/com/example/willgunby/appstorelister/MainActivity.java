package com.example.willgunby.appstorelister;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;


public class MainActivity extends ActionBarActivity implements AsyncResponse{

    Button   btnParse;
    ListView listApps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listApps = (ListView) findViewById(R.id.listApps);
        btnParse = (Button)   findViewById(R.id.btnParse);
        btnParse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnParse_Click(v);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)     return true;
        return super.onOptionsItemSelected(item);
    }


    public void btnParse_Click(View v) {
        String feedUrl = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topfreeapplications/limit=20/xml";
        //String feedUrl = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topsongs/limit=20/xml";
        new RSSGetter(this).execute(feedUrl);
    }

    @Override
    public void asyncResult(Object sender, boolean success) {
        RSSGetter rss = (RSSGetter) sender;

        if (!success){
            Log.d("ActivityMain", "RSSGetter returned fail");
        }
        else {
            ArrayAdapter<AppListing> adapter = new ArrayAdapter<>(this, R.layout.list_item, rss.getApps());
            listApps.setAdapter(adapter);
            listApps.setVisibility(View.VISIBLE);
        }
    }




}
