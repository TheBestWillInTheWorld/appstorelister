package com.example.willgunby.appstorelister;

/**
 * Created by willgunby on 21/03/15.
 */
public class AppListing {

    private String name;
    private String artist;
    private String releaseDate;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String toString(){
        return "Name: " + name + "\n" +
               "Artirst: " + artist + "\n" +
               "Release Date: " + releaseDate;
    }

}