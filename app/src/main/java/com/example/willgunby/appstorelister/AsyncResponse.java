package com.example.willgunby.appstorelister;

/**
 * Created by willgunby on 18/03/15.
 *
 */

public interface AsyncResponse {

    public void asyncResult(Object sender, boolean Success);

}
